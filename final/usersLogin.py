import cherrypy
import sys
import os
import jinja2
import mysql.connector
import mysql.connector.errors
import json
from decimal import *
import random
from passlib.apps import custom_app_context as pwd_context
#Define database variables
DATABASE_USER = 'root'
DATABASE_HOST = '127.0.0.1'
DATABASE_NAME = 'TrackStackDB'
from pyvalidate import validate,ValidationException
import utils
sys.stdout = sys.stderr # Turn off console output; it will get logged by Apache

from usersid import UsersID
class UsersLogin(object):
    def __init__(self):
        self.usersid= UsersID()
        pass
    exposed = True
    
    @validate(requires=['email', 'password','nickname'],
              types={'email':str, 'password':str,'nickname':str},
              values={'email':'^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$',
                       'password' : '(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[?!_#%^&@-]).{9,}',
                       'nickname' : '^[\w]{1,100}$'
              }
             )
    def check_params(self, email, password,nickname):
        pass
            #print 'adding user "%s:%s" with email: %s:%s password: %s:%s' % (name, type(name),email, type(email),password, type(password))
    
    @cherrypy.tools.json_out()
    def GET(self):
        response = { "user" : {},
            "errors" : []
        }
        userM={}
        if (( 'user' not in cherrypy.session) or (not cherrypy.session['user'])):
            response['errors'].append(utils.errorObject(800001));
            return response
        userM['id']=cherrypy.session['user']
        response['user']=userM
        return response
    @cherrypy.tools.json_in()
    @cherrypy.tools.json_out()
    def POST(self):
        response = { "user" : {},
            "errors" : []
        }
        try:
            username = cherrypy.request.json["nickname"]
        except:
            response['errors'].append(utils.errorObject(200001))
            return response
        try:
            password = cherrypy.request.json["password"]
        except:
            response['errors'].append(utils.errorObject(200001))
            return response
        try:
            cnx = mysql.connector.connect(user=DATABASE_USER,host=DATABASE_HOST,database=DATABASE_NAME,charset='utf8mb4')   
            cursor = cnx.cursor(dictionary = True, buffered= True)
            q="""select id,password from USERS where nickname=%(nickname)s""";
            inputDict={
                "nickname" : username
            }
            cursor.execute(q,inputDict)
            r=cursor.fetchall()
            if len(r) == 0:
                response['errors'].append(utils.errorObject(700001))
                return response
            hash=r[0]['password']
            match=pwd_context.verify(password,hash)
            password=""
            if not match:
                # username / hash do not exist in database
                response['errors'].append(utils.errorObject(700001))
                return response
            else:
                # username / password correct
                userM = {}
                userM['id'] = r[0]['id']
                cherrypy.session['user']=r[0]['id']
                response['user']=userM
        except mysql.connector.errors.Error as ex:
            response['errors'].append(utils.errorObject(000001))
        return response