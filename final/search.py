import cherrypy
import sys
import os
import jinja2
import spotipy
from externalSourceSpotify import ExternalSourceSpotify
import utils
sys.stdout = sys.stderr # Turn off console output; it will get logged by Apache
class SearchNext(object):
    def __init__(self):
        self.externalSources = [ExternalSourceSpotify()]
        pass
    exposed = True
    @cherrypy.tools.json_in()
    @cherrypy.tools.json_out()
    def PUT(self):
        print "Entered PUTNEXT"
        data = cherrypy.request.json
        response = {"results" : {},
                    "errors" : []}
        previousResults = cherrypy.session.get('previousResult');
        if  not previousResults:
           response['errors'].append(utils.errorObject(300001));
        else:
            params=None;
            if (("params" in data) and data['params']):
                params=data['params']
            results = {};
            
            for source in self.externalSources:
                results[source.name]=(source.next(previousResults[source.name],params))
                
            response['results'] = results
            cherrypy.session['previousResult'] = results
        return response

class Search(object):
    exposed=True
    def __init__(self):
        self.externalSources = [ExternalSourceSpotify()]
        self.nxt = SearchNext()
    def _cp_dispatch(self,vpath):       
        if len(vpath)>1:
            if vpath[0] == 'static':
                return vpath
        if len(vpath) == 0: #/
            print "Entered 0"
            return self
        elif len(vpath)==1: #/next
            print "Entered 1"
            token=vpath.pop()
            if token=="next":
                return self.nxt
        else:
            print "Entered neither"
        return vpath
    
    def GET(self):
        return """This is the GET method for '/search' resource"""
    
    @cherrypy.tools.json_in()
    @cherrypy.tools.json_out()
    def PUT(self):
        print "Entered PUTSEARCH"
        data = cherrypy.request.json
        response = {"results" : {},
                    "errors" : []}
        if ((not ("keyword" in data)) or (not data['keyword'])):
           response['errors'].append(utils.errorObject(200001,": keyword"));
        else:
            params=None;
            if (("params" in data) and data['params']):
                params=data['params']
            results = {};
            for source in self.externalSources:
                results[source.name]=(source.search(data['keyword'],params))
            response['results'] = results
            cherrypy.session['previousResult'] = results
        return response
application = cherrypy.Application(Search(),None) 
conf={'/':{'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
            "tools.sessions.on" : True,
            "tools.sessions.storage_type" : "file",
            "tools.sessions.storage_path" : "/var/www/final/sessions",
            "tools.sessions.timeout" : 15}
}
application.merge(conf)