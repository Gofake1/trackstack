import requests
import json
import os
s = requests.Session()
host='http://cacatuasdebutuca.com/final'   #  replace by your host here
s.headers.update({'Content-type': 'application/json'})
#success cases
''' ====User Registration====  '''

print '\n=============User Registration============='  
newUser1={
    'name' : 'AutomatedTestUser1',
    'password' : 'passwordAutomatedUser1%',
    'passwordConfirm':'passwordAutomatedUser1%',
    'email' : 'automatedUser1@email.com',
    'nickname' : 'autoUser1'
}
newUser2={
    'name' : 'AutomatedTestUser2',
    'password' : 'passwordAutomatedUser2%',
    'passwordConfirm':'passwordAutomatedUser2%',
    'email' : 'automatedUser2@email.com',
    'nickname' : 'autoUser2'
}
r = s.post(host+'/users',data=json.dumps(newUser1))
print r.status_code
if r.status_code == requests.codes.ok:
    print(r.json())
r = s.post(host+'/users',data=json.dumps(newUser2))
print r.status_code
if r.status_code == requests.codes.ok:
    print(r.json())
print '\n=============Creating Playlists============='
loginParams1 ={
    'nickname' : newUser1['nickname'],
    'password' : newUser1['password']
}
loginParams2 ={
    'nickname' : newUser2['nickname'],
    'password' : newUser2['password']
}
cookieFromServer={}

playlist1User1={
    'name' : 'Playlist1User1'
}
playlist2User1={
    'name' : 'Playlist2User1'
}
playlist1User2={
    'name' : 'Playlist1User2'
}
playlist2User2={
    'name' : 'Playlist2User2'
}
print "Creating 2 playlists to USer1\n"
r = s.post(host+'/users/login',data=json.dumps(loginParams1))
print '\n', r.status_code
if r.status_code == requests.codes.ok:
    print(r.json())
    dataRet=r.json()
    if len(dataRet['errors'])==0:
        cookieFromServer = r.cookies
        user1ID=dataRet['user']['id']
        r = s.post(host+'/users/'+str(user1ID)+'/playlists',data=json.dumps(playlist1User1),cookies=cookieFromServer)
        print '\n', r.status_code
        if r.status_code == requests.codes.ok:
            print(r.json())
        r = s.post(host+'/users/'+str(user1ID)+'/playlists',data=json.dumps(playlist2User1),cookies=cookieFromServer)
        print '\n', r.status_code
        if r.status_code == requests.codes.ok:
            print(r.json())
        r = s.post(host+'/users/logout',cookies=cookieFromServer)
        print '\n', r.status_code
        if r.status_code == requests.codes.ok:
            print(r.json())
print "Creating 2 playlists to USer2\n"
r = s.post(host+'/users/login',data=json.dumps(loginParams2))
print '\n', r.status_code
if r.status_code == requests.codes.ok:
    print(r.json())
    dataRet=r.json()
    if len(dataRet['errors'])==0:
        cookieFromServer = r.cookies
        user2ID=dataRet['user']['id']
        r = s.post(host+'/users/'+str(user2ID)+'/playlists',data=json.dumps(playlist1User2),cookies=cookieFromServer)
        print '\n', r.status_code
        if r.status_code == requests.codes.ok:
            print(r.json())
        r = s.post(host+'/users/'+str(user2ID)+'/playlists',data=json.dumps(playlist2User2),cookies=cookieFromServer)
        print '\n', r.status_code
        if r.status_code == requests.codes.ok:
            print(r.json())
        r = s.post(host+'/users/logout',cookies=cookieFromServer)
        print '\n', r.status_code
        if r.status_code == requests.codes.ok:
            print(r.json())       
print '\n=============Getting Info About User Playlists============='
print "\nGetting playlistsID of each user"
playlistsIdsUser1=[];
r = s.get(host+'/users/'+str(user1ID)+'/playlists')
print '\n', r.status_code
if r.status_code == requests.codes.ok:
    print(r.json())
    dataRet = r.json()
    if len(dataRet['errors'])==0:
        for playlist in dataRet['playlists']:
            playlistsIdsUser1.append(playlist['id'])
playlistsIdsUser2=[];
r = s.get(host+'/users/'+str(user2ID)+'/playlists')
print '\n', r.status_code
if r.status_code == requests.codes.ok:
    print(r.json())
    dataRet = r.json()
    if len(dataRet['errors'])==0:
        for playlist in dataRet['playlists']:
            playlistsIdsUser2.append(playlist['id'])
print '\n=============Inserting Songs in playlists============='
song1={
    'song_name' : 'Song1AutoTest',
    'album_name' : 'Album1AutoTest',
    'artist_name' : 'Artist1AutoTest'
}
song2={
    'song_name' : 'Song2AutoTest',
    'album_name' : 'Album2AutoTest',
    'artist_name' : 'Artist2AutoTest'
}
song3={
    'song_name' : 'Song3AutoTest',
    'album_name' : 'Album2AutoTest',
    'artist_name' : 'Artist2AutoTest'
}
r = s.post(host+'/users/login',data=json.dumps(loginParams1))
print '\n', r.status_code
if r.status_code == requests.codes.ok:
    print(r.json())
    dataRet=r.json()
    if len(dataRet['errors'])==0:
        cookieFromServer = r.cookies
        print "\nTrying to insert song in someone elses playlist"
        r = s.put(host+'/playlists/'+str(playlistsIdsUser2[0]),data=json.dumps(song1),cookies=cookieFromServer)
        print '\n', r.status_code
        if r.status_code == requests.codes.ok:
            print(r.json())
        print "\nTrying to insert song in playlst correctly"
        r = s.put(host+'/playlists/'+str(playlistsIdsUser1[0]),data=json.dumps(song1),cookies=cookieFromServer)
        print '\n', r.status_code
        if r.status_code == requests.codes.ok:
            print(r.json())
        r = s.put(host+'/playlists/'+str(playlistsIdsUser1[1]),data=json.dumps(song2),cookies=cookieFromServer)
        print '\n', r.status_code
        if r.status_code == requests.codes.ok:
            print(r.json())
        r = s.post(host+'/users/logout',cookies=cookieFromServer)
        print '\n', r.status_code
        if r.status_code == requests.codes.ok:
            print(r.json()) 

r = s.post(host+'/users/login',data=json.dumps(loginParams2))
print '\n', r.status_code
if r.status_code == requests.codes.ok:
    print(r.json())
    dataRet=r.json()
    if len(dataRet['errors'])==0:
        cookieFromServer = r.cookies
        print "\nTrying to insert song in someone elses playlist"
        r = s.put(host+'/playlists/'+str(playlistsIdsUser1[0]),data=json.dumps(song1),cookies=cookieFromServer)
        print '\n', r.status_code
        if r.status_code == requests.codes.ok:
            print(r.json())
        print "\nTrying to insert song in playlst correctly"
        r = s.put(host+'/playlists/'+str(playlistsIdsUser2[0]),data=json.dumps(song1),cookies=cookieFromServer)
        print '\n', r.status_code
        if r.status_code == requests.codes.ok:
            print(r.json())
        r = s.put(host+'/playlists/'+str(playlistsIdsUser2[0]),data=json.dumps(song2),cookies=cookieFromServer)
        print '\n', r.status_code
        if r.status_code == requests.codes.ok:
            print(r.json())
        r = s.put(host+'/playlists/'+str(playlistsIdsUser2[1]),data=json.dumps(song1),cookies=cookieFromServer)
        print '\n', r.status_code
        if r.status_code == requests.codes.ok:
            print(r.json())
        r = s.put(host+'/playlists/'+str(playlistsIdsUser2[1]),data=json.dumps(song3),cookies=cookieFromServer)
        print '\n', r.status_code
        if r.status_code == requests.codes.ok:
            print(r.json())
        r = s.post(host+'/users/logout',cookies=cookieFromServer)
        print '\n', r.status_code
        if r.status_code == requests.codes.ok:
            print(r.json())            
print '\n=============Retrieving Songs Ids in playlists============='            
songsIDs=[]
for plID in playlistsIdsUser1:
    r = s.get(host+'/playlists/'+str(plID))
    print '\n', r.status_code
    if r.status_code == requests.codes.ok:
        print(r.json())
        dataRet = r.json()
        if len(dataRet['errors'])==0: 
            for sg in dataRet['songs']:
                songsIDs.append(sg)
for plID in playlistsIdsUser2:
    r = s.get(host+'/playlists/'+str(plID))
    print '\n', r.status_code
    if r.status_code == requests.codes.ok:
        print(r.json())
        dataRet = r.json()
        if len(dataRet['errors'])==0: 
            for sg in dataRet['songs']:
                songsIDs.append(sg)
print '\n=============Retrieving Songs Info============='
for sgID in songsIDs:
    r = s.get(host+'/artists/'+str(sgID['artist_id'])+"/album/"+str(sgID['album_id'])+'/songs/'+str(sgID['song_id']))
    print '\n', r.status_code
    if r.status_code == requests.codes.ok:
        print(r.json())
print '\n=============Deleting Songs From Playlist============='
r = s.post(host+'/users/login',data=json.dumps(loginParams2))
print '\n', r.status_code
if r.status_code == requests.codes.ok:
    print(r.json())
    dataRet=r.json()
    if len(dataRet['errors'])==0:
        cookieFromServer = r.cookies
        print "\nTrying to delete song from someone elses playlist"
        r = s.delete(host+'/playlists/'+str(playlistsIdsUser1[0])+'/songs/'+str(songsIDs[0]['song_id']),cookies=cookieFromServer)
        print '\n', r.status_code
        if r.status_code == requests.codes.ok:
            print(r.json())
        print "\n ===PLaylist status before deleting==="
        r = s.get(host+'/playlists/'+str(playlistsIdsUser2[0]))
        print '\n', r.status_code
        if r.status_code == requests.codes.ok:
            print(r.json())
            dataRet = r.json()
            if len(dataRet['errors'])==0:
                songToDeleteID=dataRet['songs'][0]['song_id']
                r = s.delete(host+'/playlists/'+str(playlistsIdsUser2[0])+'/songs/'+str(songToDeleteID),cookies=cookieFromServer)
                print '\n', r.status_code
                if r.status_code == requests.codes.ok:
                    print(r.json())
                
                print "\n ===PLaylist status AFTER deleting==="
                r = s.get(host+'/playlists/'+str(playlistsIdsUser2[0]))
                print '\n', r.status_code
                if r.status_code == requests.codes.ok:
                    print(r.json())
            else:
                print "Error Trying to delete song from playlist"
        r = s.post(host+'/users/logout',cookies=cookieFromServer)
        print '\n', r.status_code
        if r.status_code == requests.codes.ok:
            print(r.json())
print '\n=============Deleting Playlist============='      
print '\n====Getting Info About User Playlists BEFORE deleting===='
r = s.get(host+'/users/'+str(user1ID)+'/playlists')
print '\n', r.status_code
if r.status_code == requests.codes.ok:
    print(r.json())
r = s.get(host+'/users/'+str(user2ID)+'/playlists')
print '\n', r.status_code
if r.status_code == requests.codes.ok:
    print(r.json())
 
print "\n ==Deleting all playlists=="
r = s.post(host+'/users/login',data=json.dumps(loginParams1))
print '\n', r.status_code
if r.status_code == requests.codes.ok:
    print(r.json())
    dataRet=r.json()
    if len(dataRet['errors'])==0:
        cookieFromServer = r.cookies
        for plID in playlistsIdsUser1:
            r = s.delete(host+'/playlists/'+str(plID))
            print '\n', r.status_code
            if r.status_code == requests.codes.ok:
                print(r.json())
        r = s.post(host+'/users/logout',cookies=cookieFromServer)
        print '\n', r.status_code
        if r.status_code == requests.codes.ok:
            print(r.json())
        
r = s.post(host+'/users/login',data=json.dumps(loginParams2))
print '\n', r.status_code
if r.status_code == requests.codes.ok:
    print(r.json())
    dataRet=r.json()
    if len(dataRet['errors'])==0:
        cookieFromServer = r.cookies
        for plID in playlistsIdsUser2:
            r = s.delete(host+'/playlists/'+str(plID))
            print '\n', r.status_code
            if r.status_code == requests.codes.ok:
                print(r.json())
        r = s.post(host+'/users/logout',cookies=cookieFromServer)
        print '\n', r.status_code
        if r.status_code == requests.codes.ok:
            print(r.json())
        
print '\n====Getting Info About User Playlists AFTER deleting===='

r = s.get(host+'/users/'+str(user1ID)+'/playlists')
print '\n', r.status_code
if r.status_code == requests.codes.ok:
    print(r.json())

r = s.get(host+'/users/'+str(user2ID)+'/playlists')
print '\n', r.status_code
if r.status_code == requests.codes.ok:
    print(r.json())
    
