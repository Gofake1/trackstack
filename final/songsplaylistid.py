import cherrypy
import sys
import os
import jinja2
import mysql.connector
import mysql.connector.errors
import json
from decimal import *
import random
from passlib.apps import custom_app_context as pwd_context
#Define database variables
DATABASE_USER = 'root'
DATABASE_HOST = '127.0.0.1'
DATABASE_NAME = 'TrackStackDB'
from pyvalidate import validate,ValidationException
import utils
sys.stdout = sys.stderr # Turn off console output; it will get logged by Apache


class SongsPlaylistID(object):
    def __init__(self):
        pass
    exposed = True
    
    @cherrypy.tools.json_out()
    def DELETE(self,playlistID,songID):
        response = {"song_playlist" : {},
                    "errors" : []}
        #( 'user' not in cherrypy.session) or 
        if ( 'user' not in cherrypy.session):
            response['errors'].append(utils.errorObject(800000));
            return response
        userID=cherrypy.session['user']
        queryPlaylist="""
                select * from PLAYLIST where id=%(playlistID)s and user_id=%(userID)s
            """
        inputPlaylist={
                'userID':userID,
                'playlistID' : playlistID
            }
        queryDelete="""
                delete from PLAYLIST_SONG where playlist_id=%(playlistID)s and song_id=%(song_id)s
        """
        inputDelete={
            'playlistID' : playlistID,
            'song_id' :songID 
        }
        try:
            cnx = mysql.connector.connect(user = DATABASE_USER,host=DATABASE_HOST,database=DATABASE_NAME)
            cursor = cnx.cursor(dictionary = True, buffered= True)
            #verifying user ownership
            cursor.execute(queryPlaylist,inputPlaylist)
            checkingPlaylists=cursor.fetchall()
            if len(checkingPlaylists)==0:
                response['errors'].append(utils.errorObject(800000));
                return response
            cursor.execute(queryDelete,inputDelete)
            rowsAffected=cursor.rowcount
            if rowsAffected==1:
                response['song_playlist']['song_id']=inputDelete['song_id']
                response['song_playlist']['playlist_id']=inputDelete['playlistID']
                cnx.commit()
                cnx.close()
            else:
                response['errors'].append(utils.errorObject(910001));
        except mysql.connector.errors.Error as ex:
            response['errors'].append(utils.errorObject(000001,": really Unknown")); 
        return response
        