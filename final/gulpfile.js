var gulp = require('gulp');
var less = require('gulp-less');

gulp.task('less', function() {
  return gulp.src('less/bootstrapWithLess.less')
    .pipe(less())
    .pipe(gulp.dest('../html/css'));
});

gulp.task('watch', function() {
  gulp.watch('less/**/*.less', ['less']);
});

gulp.task('default', ['less', 'watch']);