import cherrypy
import sys
import os
import jinja2
import mysql.connector
import mysql.connector.errors
import json
from decimal import *
import random
from passlib.apps import custom_app_context as pwd_context
#Define database variables
DATABASE_USER = 'root'
DATABASE_HOST = '127.0.0.1'
DATABASE_NAME = 'TrackStackDB'
from pyvalidate import validate,ValidationException
import utils
sys.stdout = sys.stderr # Turn off console output; it will get logged by Apache


class SongsID(object):
    def __init__(self):
        pass
    exposed = True
    @cherrypy.tools.json_out()
    def GET(self,artistID,albumID,songID):
        response = {
                    "song" : {},
                    "errors" : []}
        
        inputDict={
            'songID' : songID,
            'artistID' : artistID,
            'albumID' : albumID
        }    
        queryGetSongID = """
            select S.id as song_id,S.name as song_name, ALB.id as album_id, ALB.album_name as album_name, ART.id as artist_id, ART.name as artist_name from SONG as S
                    join ALBUM as ALB on ALB.id=S.album_id
                    join ARTISTS as ART on ART.id=ALB.artist_id
                    
                    where S.id=%(songID)s and ALB.id=%(albumID)s and ART.id=%(artistID)s 
        """
        try:
            cnx = mysql.connector.connect(user = DATABASE_USER,host=DATABASE_HOST,database=DATABASE_NAME)
            cursor = cnx.cursor(dictionary = True, buffered= True)
            cursor.execute(queryGetSongID,inputDict)
            rows=cursor.rowcount
            result=cursor.fetchone()
            response['song']=result
        except mysql.connector.errors.Error as ex:
            response['errors'].append(utils.errorObject(000001));
            
        return response