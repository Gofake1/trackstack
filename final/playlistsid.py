import cherrypy
import sys
import os
import jinja2
import mysql.connector
import mysql.connector.errors
import json
from decimal import *
import random
from passlib.apps import custom_app_context as pwd_context
#Define database variables
DATABASE_USER = 'root'
DATABASE_HOST = '127.0.0.1'
DATABASE_NAME = 'TrackStackDB'
from pyvalidate import validate,ValidationException
import utils
sys.stdout = sys.stderr # Turn off console output; it will get logged by Apache

from songsplaylist import SongsPlaylist
class PlaylistsID(object):
    def __init__(self):
        self.songsplaylist = SongsPlaylist()
        pass
    exposed = True
    def _cp_dispatch(self,vpath):       
        if len(vpath)>1:
            if vpath[0] == 'static':
                return vpath
        if len(vpath) == 0: #/playlist
            print "Entered 0"
            return vpath
        elif len(vpath)==1: #/playlist/id
            print "entered 1"
            arg1=vpath.pop()
            try:
                arg1Int = int(arg1)
                cherrypy.request.params['playlistID']=arg1
                return self
            except:
                return vpath
        elif len(vpath)==3: #/playlist/id/songs/id
            print "entered 1"
            songID=vpath.pop()
            resource2 = vpath.pop()
            playlistID = vpath.pop()
            try:
                songIDInt = int(songID)
                playlistIDInt = int(playlistID)
            except:
                return vpath
            if resource2=="songs":
                cherrypy.request.params['playlistID']=playlistID
                cherrypy.request.params['songID']=songID
                return self.songsplaylist.songsplaylistid
        else:
            print "Entered neither"
        return vpath

    @cherrypy.tools.json_in(force=False)
    @cherrypy.tools.json_out()
    def GET(self,playlistID):
        
        response = {
                    "userID" : {},
                    "songs" : [],
                    "errors" : []}
        
        inputDict={
            'playlistID' : playlistID
        }    
        queryGetSongsId = """
            select P.user_id as user_id,S.id as song_id,ALB.id as album_id, ART.id as artist_id from PLAYLIST as P 
                    join PLAYLIST_SONG as PLS on P.id=PLS.playlist_id
                    join SONG as S on S.id=PLS.song_id
                    join ALBUM as ALB on ALB.id=S.album_id
                    join ARTISTS as ART on ART.id=ALB.artist_id
                    
                    where P.id=%(playlistID)s 
        """
        try:
            cnx = mysql.connector.connect(user = DATABASE_USER,host=DATABASE_HOST,database=DATABASE_NAME)
            cursor = cnx.cursor(dictionary = True, buffered= True)
            cursor.execute(queryGetSongsId,inputDict)
            rows=cursor.rowcount
            results=cursor.fetchall()
            if len(results)>0:
                response['userID']=results[0]['user_id']
            for tuple in results:
                tuple.pop('user_id',None)
            response['songs']=results
        except mysql.connector.errors.Error as ex:
            response['errors'].append(utils.errorObject(000001));
            
        return response
    @cherrypy.tools.json_in()
    @cherrypy.tools.json_out()
    def PUT(self,playlistID):
        print "Entered PLAYLISTIDPUT"
        #name email password nick passwordConfirm
        data = cherrypy.request.json
        response = {"song_playlist" : {},
                    "errors" : []}
        #( 'user' not in cherrypy.session) or 
        if ( 'user' not in cherrypy.session):
            response['errors'].append(utils.errorObject(800000));
            return response
        userID=cherrypy.session['user']
        entryType=0 #this means the user passed names
        if(('song_name' not in data ) or ('artist_name' not in data) or('album_name' not in data) 
        or ( not data['song_name'])or ( not data['artist_name']) or ( not data['album_name'])):
            entryType=1 #this means user passed songid
            if ('song_id' not in data ) or (not data['song_id']):
                response['errors'].append(utils.errorObject(200001));
                return response
        inputDict={"id":id}
        if entryType==0:
            #Queries
            
            
            queryExistsArtist="""
                select id from ARTISTS where name=%(artist_name)s
            """
            queryExistAlbum="""
                select id from ALBUM where album_name=%(album_name)s and artist_id=%(artist_id)s
            """
            queryExistSong="""
                select id from SONG where album_id=%(album_id)s and name=%(name)s
            """
            
            #insertions
            queryInsertArtist = ("""
                    insert into ARTISTS(name) values(%(artist_name)s) 
                """)
            queryInsertAlbum = ("""
                    insert into ALBUM(album_name,artist_id) values(%(album_name)s,%(artist_id)s) 
                """)
            queryInsertSong = ("""
                    insert into SONG(name,album_id) values(%(name)s,%(album_id)s) 
                """)
            

            #dicts
            
            inputArtist={
                'artist_name':data['artist_name']
            }
            inputAlbum={
                'album_name':data['album_name'],
                'artist_id' : None #it will be get later
                
            }
            inputSong={
                'name':data['song_name'],
                'album_id': None #it will be ge later
            }
            
        queryPlaylist="""
                select * from PLAYLIST where id=%(playlistID)s and user_id=%(userID)s
            """
        queryInsertPlSong = ("""
                    insert IGNORE into PLAYLIST_SONG(playlist_id,song_id) values(%(playlist_id)s,%(song_id)s) 
                """)
        inputPlaylist={
                'userID':userID,
                'playlistID' : playlistID
            }
        inputInsertPlSong = {
                'playlist_id' : playlistID,
                'song_id' : None
            }
        '''Logic: try to insert. If succeeds, get lastrowid.
            if not, perfor  select query to get the id
        '''
        
        
        try:
            cnx = mysql.connector.connect(user = DATABASE_USER,host=DATABASE_HOST,database=DATABASE_NAME)
            cursor = cnx.cursor(dictionary = True, buffered= True)
            #verifying user ownership
            cursor.execute(queryPlaylist,inputPlaylist)
            checkingPlaylists=cursor.fetchall()
            if len(checkingPlaylists)==0:
                response['errors'].append(utils.errorObject(800000));
                return response
            if entryType==0:
                #trying Artist
                try:
                    cursor.execute(queryInsertArtist,inputArtist)
                    artistID=cursor.lastrowid
                except mysql.connector.errors.IntegrityError: #if already exists
                    #if already exists, get id
                    cursor.execute(queryExistsArtist,inputArtist)
                    artist=cursor.fetchone()
                    artistID=artist['id']
                except mysql.connector.errors.Error as ex:
                    response['errors'].append(utils.errorObject(000001,": Error Creating New Artists"))
                    return response
                inputAlbum['artist_id']=artistID
                
                #trying Album
                try:
                    cursor.execute(queryInsertAlbum,inputAlbum)
                    albumID=cursor.lastrowid
                except mysql.connector.errors.IntegrityError: #if already exists
                    #if already exists, get id
                    cursor.execute(queryExistAlbum,inputAlbum)
                    album=cursor.fetchone()
                    albumID=album['id']
                except mysql.connector.errors.Error as ex:
                    response['errors'].append(utils.errorObject(000001,": Error Creating New Album"))
                    return response
                inputSong['album_id']=albumID
                
                #trying Song
                try:
                    cursor.execute(queryInsertSong,inputSong)
                    songID=cursor.lastrowid
                except mysql.connector.errors.IntegrityError: #if already exists
                    #if already exists, get id
                    cursor.execute(queryExistSong,inputSong)
                    song=cursor.fetchone()
                    songID=song['id']
                except mysql.connector.errors.Error as ex:
                    response['errors'].append(utils.errorObject(000001,": Error Creating New Song"+str(ex))) #DEBUG ERASE BEFORe SUBMITTING
                    return response
                inputInsertPlSong['song_id']=songID
            elif entryType==1:
                inputInsertPlSong['song_id']=data['song_id']
            
            #insert sont into playlist
            try:
                cursor.execute(queryInsertPlSong,inputInsertPlSong)
                response['song_playlist']['song_id']=inputInsertPlSong['song_id']
                response['song_playlist']['playlist_id']=inputInsertPlSong['playlist_id']
                cnx.commit()
                cnx.close()
            except mysql.connector.errors.IntegrityError as ex:
                response['errors'].append(utils.errorObject(900002))
                return response
            except mysql.connector.errors.Error as ex:
                response['errors'].append(utils.errorObject(000001,": Error Inserting song to playlist"))
                return response
        except mysql.connector.errors.Error as ex:
            response['errors'].append(utils.errorObject(000001,": really Unknown"+str(ex))); #DEBUG ERASE BEFOE SUBMITTINGs 
            
        
    
        return response
    @cherrypy.tools.json_out()
    def DELETE(self,playlistID):
        print "Entered PLAYLISTIDDELETE"
        #name email password nick passwordConfirm
        response = {"playlist" : {},
                    "errors" : []}
        #( 'user' not in cherrypy.session) or 
        if ( 'user' not in cherrypy.session):
            response['errors'].append(utils.errorObject(800000));
            return response
        userID=cherrypy.session['user']
        inputDict={"id":id}
        queryPlaylist="""
                select * from PLAYLIST where id=%(playlistID)s and user_id=%(userID)s
            """
        queryDelete="""
                delete from PLAYLIST where id=%(playlistID)s
            """
        inputPlaylist={
                'userID':userID,
                'playlistID' : playlistID
            }
        inputDelete={
            'playlistID' : playlistID
        }
        try:
            cnx = mysql.connector.connect(user = DATABASE_USER,host=DATABASE_HOST,database=DATABASE_NAME)
            cursor = cnx.cursor(dictionary = True, buffered= True)
            #verifying user ownership
            cursor.execute(queryPlaylist,inputPlaylist)
            checkingPlaylists=cursor.fetchall()
            if len(checkingPlaylists)==0:
                response['errors'].append(utils.errorObject(800000));
                return response
            cursor.execute(queryDelete,inputDelete)
            rowsAffected=cursor.rowcount
            if rowsAffected==1:
                response['playlist']['playlist_id']=playlistID
                cnx.commit()
                cnx.close()
            else:
                response['errors'].append(utils.errorObject(000001));
        except mysql.connector.errors.Error as ex:
            response['errors'].append(utils.errorObject(000001,": really Unknown")); 
        return response
application = cherrypy.Application(PlaylistsID(),None) 
conf={'/':{'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
            "tools.sessions.on" : True,
            "tools.sessions.storage_type" : "file",
            "tools.sessions.storage_path" : "/var/www/final/sessions",
            "tools.sessions.timeout" : 15}
}
application.merge(conf)