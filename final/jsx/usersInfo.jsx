
var UsersInfo = React.createClass({
    getInitialState:function(){
        return{userData:null};
    },
    render : function(){
        if(this.state.userData!=null){
            return(
                <div>
                    <div>Name: {this.state.userData.name}</div>
                    <div>Nickname :{this.state.userData.nickname}</div>
                    <div>email: {this.state.userData.email}</div>
                </div>
            );
        }else{
            return null;
        }
    }
});
var userInfoComponent= React.render(<UsersInfo  />, document.getElementById("info"));
$.ajax({
    url: '/users/login',
    type : "GET",
    dataType: "json",
}).done(function(data1){
    if(data1.errors.length==0){
        $.ajax({
            url: "/users/"+data1.user.id+"/",
            type: "GET",
            dataType: "json"
        }).done(function(data){
            if(data.errors.length==0){
                userInfoComponent.setState({userData: data.user})
            }
        });
              
    }
});
