var AccessBarLogout = React.createClass({
    handleClick : function(event){
        
    },
    render: function(){
        return (
            <li onClick={this.handleClick}>Logout</li>
        );
    }
});
var AccessBar = React.createClass({
    getInitialState: function(){
        return {loggedIn : false};
    },
    componentDidMount: function() {
         var self = this;
         $.ajax({
            url: self.props.checkLoginSource,
            type : "GET",
            dataType: "json",
        }).done(function(data){
            var val = false;
            if(data.errors.length==0){
                val= true;
            }
            if (self.isMounted()) {
                self.setState({
                  loggedIn : val
                });
            }
        }).fail(function(){
            
        });
    },
    handleClick: function(i) {
        var self = this;
        switch(i){
            case 0:
                window.location.href= this.props.href[i];
                break;
            case 1:
                window.location.href= this.props.href[i];
                break;
            case 2:
                $.ajax({
                    url: this.props.href[i],
                    type : "POST",
                    dataType: "json",
                }).done(function(data){
                    console.log(data);
                    if(data.errors.length==0){
                        self.setState({loggedIn : false});
                        window.location.href = "/index.html";
                    }
                }).fail(function(){
                    console.log(data);
                });
                break;
            default:
                break;
        }
                
    },

    render: function() {
        return (
            <span id="loginBar">
                <ul>
                    {this.props.items.map(function(item, i) {
                        if(this.state.loggedIn){
                            switch(i){
                                case 0:
                                    return (<div></div>);
                                    break;
                                case 1:
                                    return (<div></div>);
                                    break;
                                case 2:
                                    return (<li onClick={this.handleClick.bind(this, i)} key={i}>{item}</li>);
                                    break;
                            }
                        }else{
                            switch(i){
                                case 0:
                                    return (<li onClick={this.handleClick.bind(this, i)} key={i}>{item}</li>);
                                    break;
                                case 1:
                                    return (<li onClick={this.handleClick.bind(this, i)} key={i}>{item}</li>);
                                    break;
                                case 2:
                                    return (<div></div>);
                                    break;
                            }
                        }
                    }, this)}
                  
                </ul>
            </span>
            
        );
    }
});
var Menu = React.createClass({
  getInitialState: function() {
    
    return {
        focused: -1
      };
  },
  componentDidMount: function(){
    var f=this.whichIsFocused(this.props.items);
    this.setState({focused:f});
  },
    whichIsFocused : function(MenuObj){
        for(var i=0,n=MenuObj.length;i<n;++i){
            if(!(typeof MenuObj[i].subMenu ==='undefined') && MenuObj[i].subMenu!=null){
                if(this.whichIsFocused(MenuObj[i].subMenu)>-1){
                    return i;
                }
            }else{
                if(!(typeof MenuObj[i].href ==='undefined')){
                    if(MenuObj[i].href==window.location.pathname.substring(1)){
                        return i;
                    }
                }
            }
        }
        
        
        return -1;
    },
  clicked: function(index) {
    if(typeof this.props.items[index].href !== 'undefined' && this.props.items[index].href!=null){ 
        window.location.href=(this.props.items[index].href);
    }
    this.setState( {focused: index} );
  },

  render: function() {
    var self = this
    var items = this.props.items;
    var sMenuComponent = null;
    if(this.state.focused>-1 &&!(typeof this.props.items[this.state.focused].subMenu ==='undefined') &&(this.props.items[this.state.focused].subMenu!=null)){
        sMenuComponent=(
            <Menu items={this.props.items[this.state.focused].subMenu} withAccessBar={false} title=""/>
        );
    }
    var acBarComponent=null;
    if(this.props.withAccessBar){
        acBarComponent=(<AccessBar href={['/usersReg.html','/usersLogin.html','/users/logout']} items={['Sign Up','Login','Logout']} checkLoginSource='/users/login' ref="accessBarMenu"/>);
    }
    var titleComponent=null;
    if (this.props.title){
        titleComponent=(<h1>TrackStack</h1>);
    }
    return (
      <div>
        {titleComponent}
        <div className="firstRow">
            <ul>
              { items.map(function(m, index) {
                var style = "";
                if (self.state.focused == index) {
                  style = "focused";
                }
                
                return <li key={items.id} className={style} onClick={self.clicked.bind(self, index)}>{m.text}</li>;
              }) }
            </ul>
            {acBarComponent}
        </div>
        <div className="secondRow">
        {sMenuComponent}
        </div>
      </div>
    );
  }
});

React.render(
    (function(){
        var mainMenuItem1={
            text: "Search",
            href: "index.html",
            subMenu: null
        };
        var mainMenuItem2={
            text : "About",
            href : "#",
            subMenu : null
        };
        var accountMenuItem1={
            text : "Settings",
            href : "usersInfo.html",
            subMenu : null
        };
        var accountMenuItem2={
            text : "Playlists",
            href : "usersPlaylist.html",
            subMenu : null
        };
        var mainMenuItem3={
            text : "Account",
            href : null,
            subMenu : [accountMenuItem1,accountMenuItem2]
        }; 
        return(<Menu title="Trackstack" withAccessBar={true} items={ [mainMenuItem1,mainMenuItem2,mainMenuItem3]} />);
    }()),document.getElementById("globalMenu")
  
);
