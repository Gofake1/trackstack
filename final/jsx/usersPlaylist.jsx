var RemoveSongButton = React.createClass({
    handleRemove: function(event){
        var self = this;
        event.stopPropagation();
        var src= "playlists/" + this.props.playlist_id+"/songs/"+this.props.song_id+"/";
        $.ajax({
            url: src,
            type : "DELETE",
            dataType: "json",
        }).done(function(data){
            self.props.refreshParent();
        }).fail(function(){
            console.log('Deleting Song AJAX Failed');
        });
        
    },
    render: function(){
        return(
            <span className="songRemoveButton glyphicon glyphicon-remove" onClick={this.handleRemove}></span>
        );    
    }
});
var SongList = React.createClass({
    handleClick : function(i){
        console.log("redirecting to search page");
        var queries = "?song="+this.props.songs[i].song_name+"&artist="+this.props.songs[i].artist_name+"&album="+this.props.songs[i].album_name;
        window.location.href=this.props.searchSource+queries;
    },
    render: function(){
        var songs=this.props.songs;
        var self=this;
        return (
            <div className="SongListDiv">
                <ul className="SongList list-group">
                    {songs.map(function(song, index) {
                        return(
                            <li key={index} className="SongItem list-group-item" onClick={self.handleClick.bind(self, index)}>
                                <span className="songInfo">
                                    <span className="songName">{song.song_name} </span> <br/>
                                    <span className="artistName">Artist: {song.artist_name} </span> <br/>
                                    <span className="albumName" >Album: {song.album_name} </span>
                                </span>
                                <span className="badge">
                                <RemoveSongButton song_id={song.song_id} playlist_id={self.props.playlist_id} refreshParent={self.props.refreshParent}/>
                                </span>
                            </li>);
                        }
                    )}
					
                </ul>
            </div>
        );
    }
});
var RemovePlaylistButton = React.createClass({
    handleRemove: function(event){
        var self = this;
        event.stopPropagation();
        var src= this.props.deletePlaylistSource + this.props.playlist_id+"/";
        $.ajax({
            url: src,
            type : "DELETE",
            dataType: "json",
        }).done(function(data){
            self.props.refreshParent();
        }).fail(function(){
            console.log('Deleting Playlist AJAX Failed');
        });
        
    },
    render: function(){
        return(
            <span className="glyphicon glyphicon-remove" onClick={this.handleRemove}></span>
        );    
    }
});
var PlaylistItem = React.createClass({ 
    render: function(){
        var self=this;
        var rmvButton = (<RemovePlaylistButton playlist_id={this.props.playlist.id} deletePlaylistSource="/playlists/" refreshParent={this.props.refreshParent} />);
        if(typeof this.props.songs === 'undefined'){
            return(
                    <li className="playListItem list-group-item">
                        <span className="playListName" onClick={self.props.parentHandleClick}>{this.props.playlist.name} 
                        
                        </span>
                        <span className="badge">{rmvButton}</span>
                    </li>
            );
        }else{
            return (               
                    <li className="playListItem list-group-item" >
                        <span className="playListName" onClick={self.props.parentHandleClick}>{this.props.playlist.name}
                        </span>
                        <span className="badge">{rmvButton}</span>
                        <SongList songs={this.props.songs} playlist_id={this.props.playlist.id} refreshParent={this.props.refreshParent} searchSource="/" />                        
                    </li>
            );
        }
    }
});
var AddPlaylistPanel = React.createClass({
    getInitialState: function(){
        return ({value : ""});
    
    },
    onChangeInput: function(event){
        this.setState({value: event.target.value});
    },
    handleSubmission:function(event){
        var self = this;
        event.preventDefault();
        var src = this.props.postPlaySource + this.props.user_id+"/playlists/";
         $.ajax({
            url: src,
            type : "POST",
            contentType : "application/json",
            dataType: "json",
            data : JSON.stringify({
                name:  this.state.value
            })
        }).done(function(data){
            console.log(data);
            self.props.refreshParent();
        }).fail(function(){
            console.log("insertPLaylist AJAX failed");
        });   
    },
    render: function(){
        return (
            <div className="addPlaylistPanelClass">
                <form onSubmit={this.handleSubmission}>
                    <div className="newPlaylistInput">
                        <input name="name"  placeholder="Insert New Playlist Name" type="text" ref="name" value={this.state.value} onChange={this.onChangeInput}/>
                    </div>
                </form>
            </div>
        );
    }
});
var PlaylistList = React.createClass({
    getInitialState : function(){
        return {
            items : [],
            showingKey : -1,
            showingData : null,
            addingPlaylist : false
            };
    },
    refresh: function() {
         var self = this;
         $.ajax({
            url: self.props.getPlayListSource,
            type : "GET",
            dataType: "json"
        }).done(function(data){
            if(data.errors.length==0){
                if (self.isMounted()) {
                    self.setState({
                        items : data.playlists,
                        showingKey : -1,
                        showingData : null,
                        addingPlaylist : false
                    });
                }
            }
            
        }).fail(function(){
            
        });
    },
    componentDidMount: function() {
         this.refresh();
    },
    handleClickPlItem: function(childClicked){
        console.log("Query Server For songs");
        var self = this;
        var playlistClicked = childClicked;
        var src = this.props.getPlaylistSongsSourceBase +this.state.items[childClicked].id;
        $.ajax({
            url: src,
            type : "GET",
            dataType: "json",
        }).done(function(data){
            console.log(data);
            var everythingOK=true;
            var ajaxesArray = Array();
            var sgs=Array();
            if (data.errors.length==0){
                ajaxesArray=data.songs.map(function (song,index){
                    var srcSongs="artists/"+song.artist_id+"/album/"+song.album_id+"/songs/"+song.song_id+"/";
                    return (
                        $.ajax({
                            url: srcSongs,
                            type : "GET",
                            dataType: "json",
                        }).done(function(data){
                            console.log(data)
                            if(data.errors.length==0){
                                sgs.push(data.song);
                            }else{
                                everythingOK=false;
                            } 
                        })
                    );
                });
                var doneFunc = function(){  
                    $.each(arguments, function(index, responseData){
                    });
                    if (everythingOK){
                        self.setState({
                            showingKey : playlistClicked,
                            showingData : sgs
                        });
                    }
                };
                $.when.apply($,ajaxesArray).done(doneFunc);
            }
            
        }).fail(function(){
            console.log("Fetching Songs Info: First Ajax Failed");
        });
    },
    handleAddClick : function(){
        console.log("AddCLick handle");
        this.setState({addingPlaylist : !this.state.addingPlaylist});
    },
    render: function(){
        var self=this;
        var playlists = this.state.items;
        var playlistShowingID = this.state.showingKey;
        var playlistShowingData = this.state.showingData;
        var addPlaylistPan= (this.state.addingPlaylist) ? (<AddPlaylistPanel user_id={this.props.user_id} refreshParent={this.refresh.bind(this)} postPlaySource="/users/"/>) : "";
        return (
            <div className="playListListDiv panel panel-default">
                <div className="panel-heading myPanelHeader">
                <span className="h2">PlayLists</span>
                <span class="btn-group">
                    <button  type="button" className="refreshPlaylist myButton btn btn-default" onClick={this.refresh}><span className="glyphicon glyphicon-refresh"></span></button>
                    
                        <button type="button" className="addPlaylist myButton btn btn-default" onClick={this.handleAddClick}><span className="glyphicon glyphicon-plus"></span></button>
                        
                   
                </span>
                </div>
                
                <div className="panel-body">
                    {addPlaylistPan}
                </div>
                
                <ul className="playListList list-group">
                    {playlists.map(function(playlistI, index) {
                        var boundClick = self.handleClickPlItem.bind(self,index);
                        if(playlistShowingID == index){ 
                            return(
                               
                               <PlaylistItem playlist={playlistI}  songs={playlistShowingData} parentHandleClick={boundClick} key={index} refreshParent={self.refresh}/>
                            );
                        }else{
                            return(
                               
                               <PlaylistItem playlist={playlistI} parentHandleClick={boundClick} key={index} refreshParent={self.refresh}/>
                           );
                        }
                    }
                    )}
					
                </ul>
            </div>
        );
    }
});
var MessageBox = React.createClass({
    getInitialState : function(){
        return {idx : -1};
    },
    render: function(){
        var msg = (this.state.idx>=0 && this.state.idx<this.props.messages.length) ? this.props.messages[this.state.idx] : '';
        return <div>{msg}</div>;
    }    
});

$.ajax({
    url: '/users/login',
    type : "GET",
    dataType: "json",
}).done(function(data){
    if(data.errors.length==0){
        var getPlSource = "users/" + data.user.id+"/playlists/";
        React.render(<PlaylistList user_id ={data.user.id} getPlayListSource={getPlSource} getPlaylistSongsSourceBase="/playlists/"/>, document.getElementById("playLists"));
    }
}).fail(function(){
    
});
