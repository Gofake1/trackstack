var NickNameInputClass = React.createClass({
    getInitialState : function(){
        return {value : ''};
    },
    handleChange : function(event){
        this.setState({value : event.target.value});
    },
    render: function(){
        return (
            <div className="form-group">
                <label htmlFor="nicknameInput">Nickname: </label>
                <input className="form-control" id="nicknameInput" type="text" name= "nickname" onChange = {this.handleChange} required/>
            </div>
        );
    }
});
var PasswordInputClass = React.createClass({
    getInitialState : function(){
        return {value : ''};
    },
    handleChange : function(event){
        this.setState({value : event.target.value});
    },
    render: function(){
        return (
            <div className="form-group">
                <label htmlFor="passwordInput">Password: </label>
                <input className="form-control" id="passwordInput" type="password" name= "password" onChange = {this.handleChange} required/>
            </div>
        );
    }
});
var MessageBox = React.createClass({
    getInitialState : function(){
        return {idx : -1};
    },
    render: function(){
        var msg = (this.state.idx>=0 && this.state.idx<this.props.messages.length) ? this.props.messages[this.state.idx] : '';
        return <div>{msg}</div>;
    }    
});
var LoginForm = React.createClass({
    handleSubmit: function(event){
        event.preventDefault();
        var self = this;
        $.ajax({
            url: this.props.act,
            type : "POST",
            contentType : "application/json",
            dataType: "json",
            data : JSON.stringify({
                nickname : this.refs.nick.state.value,
                password : this.refs.pass.state.value
            })
        }).done(function(data){
            console.log(data);
            if(data.errors.length==0){
                self.refs.msgBox.setState({idx:1});
                window.location.href = "/index.html";
            }else{
                self.refs.msgBox.setState({idx:0});
            }
        }).fail(function(){
            console.log(data);
            self.refs.msgBox.setState({idx:2});
        });
    },
    render: function() {
    return (
        <div>
            <MessageBox messages={['Login or Password Invalid','Login Successful','Request Error']} ref= "msgBox"/>
            <form className="form-horizontal" action={this.props.act} onSubmit={this.handleSubmit}>
                <NickNameInputClass ref="nick"/>
                <PasswordInputClass ref="pass"/>
                <input className="btn btn-default" type="submit" value="Login"/>
            </form>
        </div>
    );
    }
});


React.render(<LoginForm  act="users/login"/>, document.getElementById("loginForm"));
