
var SearchBox = React.createClass({
  handleChange: function() {
    this.props.onUserInput(this.refs.searchBox.getDOMNode().value);
  },

  render: function() {
    return (
      <form>
        <div id="searchbox">
        <input
          type="text"
          placeholder="Search"
          value={this.props.query}
          onChange={this.handleChange}
          ref="searchBox"
        />
        </div>
      </form>
    );
  }
});

var SearchResultRow = React.createClass({
  handleSubmit: function() {
    if (this.props.user == "") {
      alert("You must be logged in.");
      return;
    }
    this.props.onAddTrack(this.props.track);
    console.log(this.props.track);
  },

  render: function() {
    var href = this.props.track.href;
    var name = this.props.track.name;
    var album = this.props.track.album;
    var artists = new String();
    this.props.track.artists.forEach(function(artist) {
      artists += (artist.name + ", ");
    }.bind(this));
    artists = artists.slice(0, -2);
    return (
      <tr>
        <td><a href={href}>{name}</a></td>
        <td>{album}</td>
        <td>{artists}</td>
        <td><button type="button" onClick={this.handleSubmit}>+</button></td>
      </tr>
    );
  }
});

var SearchResultsTable = React.createClass({
  getInitialState: function() {
    return { tracks: null };
  },

  componentWillReceiveProps: function(nextProps) {
    var dataObject = { "keyword": nextProps.query };
    $.ajax({
      url: this.props.source,
      type: "PUT",
      data: JSON.stringify(dataObject),
      contentType: "application/json",
      success: function(result) {
        if (!result) {
          return;
        }
        var tracks = result.results.spotify.tracks.items.map(function(track) {
          return {
            href: track.href,
            name: track.name,
            artists: track.artists,
            album: track.album.name
          };
        });
        this.setState( {tracks: tracks} );
      }.bind(this)
    });
  },

  render: function() {
    if (this.state.tracks) {
      var rows = [];
      this.state.tracks.forEach(function(track) {
        rows.push(<SearchResultRow track={track} user={this.props.user} onAddTrack={this.props.onAddTrack}/>);
      }.bind(this));
      
      return (
        <div>
        <table>
          <tbody> {rows} </tbody>
        </table>
        </div>
      );
    }
    else {
      return <div></div>;
    }
  }
});

var PlaylistCell = React.createClass({
  handleSubmit: function() {
    console.log("Added track " + this.props.addedTrack);
    var dataObject = { "song_id": this.props.addedTrack };
    $.ajax({
      url: "https://trackstacknd.com/playlists/" + this.props.playlist.id,
      type: "PUT",
      data: JSON.stringify(dataObject),
      contentType: "application/json",
      success: function(result) {
        if (!result) {
          return;
        }
        alert("Song added to playlist " + this.props.playlist.name);
      }.bind(this)
    });
  },

  render: function() {
    return (
      <td onClick={this.handleSubmit}>{this.props.playlist.name}</td>
    );
  }
})

var PlaylistList = React.createClass({
  getInitialState: function() {
    return ({
      playlists: [],
      addedTrack: ""
    });
  },

  componentDidMount: function() {
    var self = this;
    $.get("https://trackstacknd.com/users/" + this.props.user + "/playlists", function(data) {
      if (data.errors.length == 0) {
        self.setState({playlists: data.playlists});
      }
    });
  },

  componentWillReceiveProps: function(nextProps) {
    this.state.addedTrack = nextProps.addedTrack
  },

  render: function() {
    var cells = [];
    this.state.playlists.forEach(function(playlist) {
      cells.push(<PlaylistCell playlist={playlist} addedTrack={this.state.addedTrack}/>)
    }.bind(this));
    return (
      <div>
      <table>
        <tbody>
         <tr> {cells} </tr>
        </tbody>
      </table>
      </div>
    );
  }
})

var SearchPage = React.createClass({
  getInitialState: function() {
    return {
      query: "",
      user: "",
      addedTrack: "",
      addingTrack: false
    };
  },

  componentDidMount :function(){
    var artistQuery = this.getUrlParameter('artist');
    var songQuery = this.getUrlParameter('song');
    var albumQuery = this.getUrlParameter('album');
    var q="";
    var self = this;
    if (artistQuery!=null){
        q=q+" "+artistQuery;
    }
    if (songQuery!=null){
        q=q+" "+songQuery;
    }
    if (albumQuery!=null){
        q=q+" "+albumQuery;
    }
    if (q!=""){
        this.setState({query : q});
    }
    $.get(this.props.login, function(data) {
      if (data.errors.length==0) {
        self.setState({user: data.user.id});
      }
    });
  },

  handleUserInput: function(query) {
    this.setState( {query: query} );
  },

  handleAddTrack: function(track) {
    this.setState({
      addedTrack: track,
      addingTrack: true
    });
  },

  getUrlParameter: function (sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) 
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) 
        {
            return decodeURIComponent(sParameterName[1]);
        }
    }
  },

  render: function() {
    return (
      <div>
        <SearchBox 
          query={this.state.query}
          onUserInput={this.handleUserInput}
        />
        { this.state.addingTrack ? 
          <PlaylistList
          addedTrack={this.state.addedTrack}
          user={this.state.user}
          /> : null
        }
        <SearchResultsTable 
          query={this.state.query}
          user={this.state.user}
          source={this.props.source}
          onAddTrack={this.handleAddTrack}
        />
      </div>
    );
  }
});

React.render(<SearchPage source="https://trackstacknd.com/search" login="https://trackstacknd.com/users/login" />, document.getElementById("search"));
