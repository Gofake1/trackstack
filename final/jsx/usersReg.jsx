var MessageBox = React.createClass({
    getInitialState : function(){
        return {idx : -1};
    },
    render: function(){
        var msg = (this.state.idx>=0 && this.state.idx<this.props.messages.length) ? this.props.messages[this.state.idx] : '';
        return <div className="messageBox">{msg}</div>;
    }    
});
var NickNameInputClass = React.createClass({
    getInitialState : function(){
        return {value : ''};
    },
    handleChange : function(event){
        this.setState({value : event.target.value});
    },
    render: function(){
        return (
            <div className="form-group">
                <label htmlFor="nicknameInput">Nickname: </label>
                <input className="form-control" id="nicknameInput" type="text" name= "nickname" onChange = {this.handleChange} value={this.state.value} maxLength="100" required/>
                <MessageBox messages={['Invalid Size of Nickname']} ref= "msgBox"/>
            </div>
        );
    }
});
var NameInputClass = React.createClass({
    getInitialState : function(){
        return {value : ''};
    },
    handleChange : function(event){
        this.setState({value : event.target.value});
    },
    render: function(){
        return (
            <div className="form-group">
                <label htmlFor="nameInput">Name: </label>
                <input className="form-control" id="nameInput" type="text" name= "name" onChange = {this.handleChange} value={this.state.value} maxLength="100"/>
            </div>
        );
    }
});
var EmailInputClass = React.createClass({
    getInitialState : function(){
        return {value : ''};
    },
    handleChange : function(event){
        this.setState({value : event.target.value});
    },
    render: function(){
        return (
            <div className="form-group">
                <label htmlFor="emailInput">Email: </label>
                <input className="form-control" id="emailInput" type="email" name="email" onChange = {this.handleChange} value={this.state.value} maxLength="100" required/>
                <MessageBox messages={['Invalid Email']} ref= "msgBox"/>
            </div>
        );
    }
});
var PasswordInputClass = React.createClass({
    getInitialState : function(){
        return {value : ''};
    },
    handleChange : function(event){
        this.setState({value : event.target.value});
    },
    render: function(){
        return (
            <div className="form-group">
                <label htmlFor="passwordInput">Password: </label>
                <input className="form-control" id="passwordInput" type="password" name= "password" onChange = {this.handleChange} value={this.state.value} required/>
               <MessageBox messages={['Password not Secure. It must contain: more than 9 characters, at learst: one upper case leter,onelower case letter, one number and one special character']} ref= "msgBox"/>
            </div>
        );
    }
});
var PasswordConfirmInputClass = React.createClass({
    getInitialState : function(){
        return {value : ''};
    },
    handleChange : function(event){
        this.setState({value : event.target.value});
    },
    render: function(){
        return (
            <div className="form-group">
                <label htmlFor="passwordConfirmInput">Confirm Password: </label>
                <input className="form-control" id="passwordConfirmInput" type="password" name="passwordConfirm" onChange = {this.handleChange} value={this.state.value} required/>
                <MessageBox messages={['Passwords do not Match']} ref= "msgBox"/>
            </div>
        );
    }
});


var RegForm = React.createClass({
    validate: function(){
        emailRegEx= /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;
        passwordRegEx= /(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[?!_#%^&@-]).{9,}/;
        nicknameRegEx= /^[\w]{1,100}$/;
        var isValid=true;
        if(!emailRegEx.test(this.refs.email.state.value)){
            this.refs.email.refs.msgBox.setState({idx:0});
            isValid=false;
        }else{
            this.refs.email.refs.msgBox.setState({idx:-1});
        }
        
        if (!passwordRegEx.test(this.refs.pass.state.value)){
            this.refs.pass.refs.msgBox.setState({idx:0});
            isValid = false;
        }else{
            this.refs.pass.refs.msgBox.setState({idx:-1});
        }
        
        if(this.refs.passConf.state.value != this.refs.pass.state.value){
            this.refs.passConf.refs.msgBox.setState({idx:0});
            isValid=false;
        }else{
            this.refs.passConf.refs.msgBox.setState({idx:-1});
        }
        if (!nicknameRegEx.test(this.refs.nick.state.value)){
            this.refs.nick.refs.msgBox.setState({idx:0});
            isValid = false;
        }else{
            this.refs.nick.refs.msgBox.setState({idx:-1});
        }
        return isValid;
    },
    handleSubmit: function(event){
        event.preventDefault();
        if (this.validate()){    
            var self = this;
            $.ajax({
                url: this.props.act,
                type : "POST",
                contentType : "application/json",
                dataType: "json",
                data : JSON.stringify({
                    nickname : this.refs.nick.state.value,
                    password : this.refs.pass.state.value,
                    passwordConfirm : this.refs.passConf.state.value,
                    name : this.refs.name.state.value,
                    email : this.refs.email.state.value
                })
            }).done(function(data){
                console.log(data);
                if(data.errors.length==0){
                    self.refs.msgBox.setState({idx:1});
                    window.location.href = "/index.html";
                }else{
                    switch(data.errors[0].code){
                        case 000001:
                            self.refs.msgBox.setState({idx:0});
                            break;
                        case 600000:
                            self.refs.msgBox.setState({idx:3});
                            break;
                        default:
                            self.refs.msgBox.setState({idx:0});
                    }
                }
            }).fail(function(){
                console.log(data);
                self.refs.msgBox.setState({idx:2});
            });
        }
    },
    render: function() {
    return (
        <div>
            <MessageBox messages={['Unexpected Server Error. Contact the system Adm.','Login Successful','Request Error','User Already Exists. Try Another email and/or nickname']} ref= "msgBox"/>
            <form className="form-horizontal" action={this.props.act} onSubmit={this.handleSubmit}>
                <NameInputClass ref="name" />
                <NickNameInputClass ref="nick"/>
                <EmailInputClass ref="email" />
                <PasswordInputClass ref="pass"/>
                <PasswordConfirmInputClass ref="passConf"/>
                <input className="btn btn-default" type="submit" value="Sign Up"/>
            </form>
        </div>
    );
    }
});


React.render(<RegForm  act="users"/>, document.getElementById("regForm"));
