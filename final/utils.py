import collections
import json
from types import IntType,StringType
class Errors(object):
    def __init__(self):
        self.definedErrors={
            000001: 'Unknown Database Error',
            200001: 'Required Parameter Missing',
            300001: 'No Previous Search Detected',
            600000: 'User Already Exists',
            600001: 'Password do not Match',
            600002: 'Invalid Parameter Value',
            700001: 'Incorrect nickname or password',
            800000: 'Unauthorized Operation',
            800001: "No User Logged In",
            900001: "Duplicated Playlist Name",
            900002: "Song does not exist",
            910001: "Song does not in Playlist"
        }

def errorJSON(code,message=''):
    ''' Return an error object
    Error object is of the form:
    {  'code' : error code (integer),
       'message' : error message (string)}'''

    
    assert type(code) is IntType, "code needs to be an integer: %r" % code
    assert type(message) is StringType, "message needs to be a string: %r" % message
    assert code in Errors().definedErrors, "Error Code is Not Defined"
    
    # create error object
    error={'code' : code,
           'message' : Errors().definedErrors[code]+message}
    errors={'errors':[error]}
    return json.dumps(errors)
    
def errorObject(code,message=''):
    
    assert type(code) is IntType, "code needs to be an integer: %r" % code
    assert type(message) is StringType, "message needs to be a string: %r" % message
    assert code in Errors().definedErrors, "Error Code is Not Defined"
    
    # create error object
    error={'code' : code,
           'message' : Errors().definedErrors[code]+message}
    return error
