import cherrypy
import sys
import os
import jinja2
import mysql.connector
import mysql.connector.errors
import json
from decimal import *
import random
from passlib.apps import custom_app_context as pwd_context
#Define database variables
DATABASE_USER = 'root'
DATABASE_HOST = '127.0.0.1'
DATABASE_NAME = 'TrackStackDB'
from pyvalidate import validate,ValidationException
import utils
sys.stdout = sys.stderr # Turn off console output; it will get logged by Apache
from artistsid import ArtistsID

class Artists(object):
    def __init__(self):
        self.artistsid = ArtistsID()
        pass
    exposed = True
    def _cp_dispatch(self,vpath):       
        if len(vpath)>1:
            if vpath[0] == 'static':
                return vpath
        if len(vpath)==5: #/artistID/resource2/resource2ID/resource3/resource3ID
            #/id/album/id/songs/id
            print "ENTERED 5"
            resource3ID=vpath.pop()
            resource3=vpath.pop()
            resource2ID=vpath.pop()
            resource2=vpath.pop()
            artistID=vpath.pop()
            print (artistID,"/",resource2,"/",resource2ID,"/",resource3,"/",resource3ID)
            try:
                artistIDInt = int(artistID)
                cherrypy.request.params['artistID']=artistID
            except:
                return vpath
            
            if resource2=="album":
                try:
                    albumIDInt = int(resource2ID)
                    cherrypy.request.params['albumID']=resource2ID
                except:
                    return vpath
                
                if resource3=="songs":
                    try:
                        songInt = int(resource3ID)
                        cherrypy.request.params['songID']=resource3ID
                        
                    except:
                        return vpath
                    print "reached end of parsing"
                    return self.artistsid.album.albumid.songs.songsid
            return vpath
        else:
            print "Entered neither"
        return vpath

application = cherrypy.Application(Artists(),None) 
conf={'/':{'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
            "tools.sessions.on" : True,
            "tools.sessions.storage_type" : "file",
            "tools.sessions.storage_path" : "/var/www/final/sessions",
            "tools.sessions.timeout" : 15}
}
application.merge(conf)