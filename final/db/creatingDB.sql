create table USERS(
    id int auto_increment,
    name varchar(100),
    email varchar(100) not null,
    password varchar(150) not null,
    nickname varchar(100) not null,
    primary key(id),
    unique(email),
    unique(nickname)
    );
    
create table ARTISTS(
    id int auto_increment,
    name varchar(200) not null,
    primary key(id),
    unique(name)
    );
    
create table GENRE(
    id int auto_increment,
    genre_name varchar(25) not null,
    primary key(id),
    unique (genre_name)
    );
    

    
 create table ARTIST_GENRE(
    artist_id int,
    genre_id int,
    primary key(artist_id,genre_id),
    foreign key(artist_id) references ARTISTS(id) on delete cascade,
    foreign key(genre_id) references GENRE(id)
    );
create table ALBUM(
    id int auto_increment,
    album_name varchar(100) not null,
    artist_id int not null,
    primary key(id),
    foreign key(artist_id) references ARTISTS(id),
    unique(album_name,artist_id)
    );
create table SONG(
    id int auto_increment,
    name varchar(200) not null,
    album_id int not null,
    primary key(id),
    foreign key(album_id) references ALBUM(id),
    unique (name,album_id)
    );    
  create table LIKES(
    user_id int,
    song_id int,
    primary key(user_id,song_id),
    foreign key(user_id) references USERS(id) on delete cascade,
    foreign key(song_id) references SONG(id) 
    );  
 create table FOLLOWING_ARTIST(
    user_id int,
    artist_id int,
    primary key(user_id,artist_id),
    foreign key(user_id) references USERS(id) on delete cascade,
    foreign key(artist_id) references ARTISTS(id) 
    );
create table PLAYLIST(
    id int auto_increment,
    name varchar(100),
    user_id int,
    primary key(id),
    foreign key(user_id) references USERS(id) on delete cascade,
    unique(user_id,name)
    );
create table PLAYLIST_SONG(
    playlist_id int,
    song_id int,
    primary key(playlist_id,song_id),
    foreign key(playlist_id) references PLAYLIST(id) on delete cascade,
    foreign key(song_id) references SONG(id)
    );
create table PLAYLIST_SUBSCRIPTION(
    user_id int,
    playlist_id int,
    primary key(user_id,playlist_id),
    foreign key(user_id) references USERS(id) on delete cascade,
    foreign key(playlist_id) references PLAYLIST(id) on delete cascade
    );