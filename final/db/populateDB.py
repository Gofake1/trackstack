#Use this script to populate the |restaurant| table of the database

import mysql.connector
import mysql.connector.errors
import json
from decimal import *
import random
from passlib.apps import custom_app_context as pwd_context
#Define database variables
DATABASE_USER = 'root'
DATABASE_HOST = '127.0.0.1'
DATABASE_NAME = 'TrackStackDB'

#Create connection to MySQL
cnx = mysql.connector.connect(user=DATABASE_USER, host=DATABASE_HOST, database=DATABASE_NAME,use_unicode=True)
cursor = cnx.cursor(dictionary=True)

#adding User
hash = pwd_context.encrypt('1234')
insertUserQuery = """insert into USERS(name,email,password,nickname) values('Test User','dummyUser@email.com',%(password)s,'DummyNickName')
    """
inputDict={'password' : hash}
cursor.execute(insertUserQuery,inputDict)

hash = pwd_context.encrypt('1234')
insertUserQuery = """insert into USERS(name,email,password,nickname) values('Test User2','dummyUser2@email.com',%(password)s,'DummyNickName2')
    """
inputDict={'password' : hash}
cursor.execute(insertUserQuery,inputDict)
hash = pwd_context.encrypt('1234')
insertUserQuery = """insert into USERS(name,email,password,nickname) values('Test User3','dummyUser3@email.com',%(password)s,'DummyNickName3')
    """
inputDict={'password' : hash}
cursor.execute(insertUserQuery,inputDict)
#adding Genres
genresList = ['POP','Electronic','Rock','RnB','MPB'];
for genre in genresList:
    genresListQuery = """insert into GENRE(genre_name) values(UPPER(%(gName)s))"""
    inputDict={
        'gName' : genre 
    }
    cursor.execute(genresListQuery,inputDict)
cnx.commit()
cnx.close()

