#Use this script to create the database tables
#This script does NOT populate the tables with any data

import mysql.connector
import mysql.connector.errors

#Define database variables
DATABASE_USER = 'root'
DATABASE_HOST = '127.0.0.1'
DATABASE_NAME = 'TrackStackDB'

#Create connection to MySQL
cnx = mysql.connector.connect(user=DATABASE_USER, host=DATABASE_HOST)
cursor = cnx.cursor()

###################################
## Create DB if it doesn't exist ##
###################################
dropDB=(("DROP DATABASE IF EXISTS %s ")%(DATABASE_NAME))
createDB = (("CREATE DATABASE %s DEFAULT CHARACTER SET latin1") % (DATABASE_NAME))
cursor.execute(dropDB)
cursor.execute(createDB)

#########################
## Switch to feednd DB ##
#########################

useDB = (("USE %s") % (DATABASE_NAME))
cursor.execute(useDB)

###########################
## Drop all tables first ##
###########################

#Done by dropping th entire database


########################
## Create tables next ##
########################

file = open('./creatingDB.sql', 'r')
sql = s = " ".join(file.readlines())
results = cursor.execute(sql,multi=True)
for result in results:
  if result.with_rows:
    print("Rows produced by statement '{}':".format(
      result.statement))
    print(result.fetchall())
  else:
    print("Number of rows affected by statement '{}': {}".format(
      result.statement, result.rowcount))



        
#Commit the data and close the connection to MySQL
cnx.commit()
cnx.close()

##references: http://www.quora.com/How-can-I-execute-a-sql-file-in-Python