import cherrypy
import sys
import os
import jinja2
import mysql.connector
import mysql.connector.errors
import json
from decimal import *
import random
from passlib.apps import custom_app_context as pwd_context
#Define database variables
DATABASE_USER = 'root'
DATABASE_HOST = '127.0.0.1'
DATABASE_NAME = 'TrackStackDB'
from pyvalidate import validate,ValidationException
import utils
sys.stdout = sys.stderr # Turn off console output; it will get logged by Apache


class Playlists(object):
    def __init__(self):

        pass
    exposed = True
    
  
    @cherrypy.tools.json_out()
    def GET(self,userID):
        response = {"playlists" : [],
                    "errors" : []}

        
        
        inputDict={
            'user_id': userID
        }
        queryGETPlaylist = ("""
                select id,name,user_id from PLAYLIST where user_id =%(user_id)s
        """)
        try:
            cnx = mysql.connector.connect(user = DATABASE_USER,host=DATABASE_HOST,database=DATABASE_NAME)
            cursor = cnx.cursor(dictionary = True, buffered= True)
            cursor.execute(queryGETPlaylist,inputDict)
            r=cursor.fetchall() 
            response['playlists']=r
            return response 
        except mysql.connector.errors.Error as ex:
            response['errors'].append(utils.errorObject(000001));
            
        return response
    @cherrypy.tools.json_in(force=False)
    @cherrypy.tools.json_out()
    def POST(self,userID):
        print "Entered playlistPOST"
        data = cherrypy.request.json
        response = {"playlist" : {},
                    "errors" : []}
        if (( 'user' not in cherrypy.session) or (cherrypy.session['user']!=int(userID))):
            response['errors'].append(utils.errorObject(800000))
            return response            
        if  ((not "name" in data) or  (not data["name"])):
           response['errors'].append(utils.errorObject(200001))
        else:
            
            inputDict={
                'name' : data['name'],
                'user_id': userID
            }
            queryInsertPlaylist = ("""
                    insert into PLAYLIST(name,user_id) values(%(name)s,%(user_id)s)
            """)
            try:
                cnx = mysql.connector.connect(user = DATABASE_USER,host=DATABASE_HOST,database=DATABASE_NAME)
                cursor = cnx.cursor(dictionary = True, buffered= True)
                cursor.execute(queryInsertPlaylist,inputDict)
                rows=cursor.rowcount
                if rows!=1:
                    response['errors'].append(utils.errorObject(00001,': Error Inserting'));
                else:       
                    playlistM={}
                    playlistM['id']=cursor.lastrowid;
                    playlistM['name']=data['name']
                    playlistM['user_id']=userID
                    response['playlist']=playlistM
                    cnx.commit()
                    cnx.close()
                    return response 
            except mysql.connector.errors.IntegrityError:
                response['errors'].append(utils.errorObject(900001));
            except mysql.connector.errors.Error as ex:
                response['errors'].append(utils.errorObject(000001));
            
        return response