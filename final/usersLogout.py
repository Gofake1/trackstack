import cherrypy
import sys
import os
import jinja2
import mysql.connector
import mysql.connector.errors
import json
from decimal import *
import random
from passlib.apps import custom_app_context as pwd_context
#Define database variables
DATABASE_USER = 'root'
DATABASE_HOST = '127.0.0.1'
DATABASE_NAME = 'TrackStackDB'
from pyvalidate import validate,ValidationException
import utils
sys.stdout = sys.stderr # Turn off console output; it will get logged by Apache

from usersid import UsersID
class UsersLogout(object):
    def __init__(self):
        self.usersid= UsersID()
        pass
    exposed = True
    
    
    def GET(self):
        output_format = cherrypy.lib.cptools.accept(['text/html'])
        if output_format == 'text/html':
            return "Put Logout Page here"
        else:
            return "it should never have returned this"
    @cherrypy.tools.json_out()
    def POST(self):
        response = { "user" : {},
            "errors" : []
        }
        userM={}
        if (( 'user' not in cherrypy.session)):
            response['errors'].append(utils.errorObject(800001));
            return response
        userM['id']=cherrypy.session['user']
        cherrypy.lib.sessions.expire()
        response['user']=userM
        return response