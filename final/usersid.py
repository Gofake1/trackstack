import cherrypy
import sys
import os
import jinja2
import mysql.connector
import mysql.connector.errors
import json
from decimal import *
import random
from passlib.apps import custom_app_context as pwd_context
#Define database variables
DATABASE_USER = 'root'
DATABASE_HOST = '127.0.0.1'
DATABASE_NAME = 'TrackStackDB'
from pyvalidate import validate,ValidationException
import utils
sys.stdout = sys.stderr # Turn off console output; it will get logged by Apache

from playlists import Playlists
class UsersID(object):
    def __init__(self):
        self.playlists=Playlists()
        pass
    exposed = True
    @validate(requires=['email'],
              types={'email':str},
              values={'email':'^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$'
              }
             )
    def check_email(self, email):
        pass
            #print 'adding user "%s:%s" with email: %s:%s password: %s:%s' % (name, type(name),email, type(email),password, type(password))
    @validate(requires=['nickname'],
              types={'nickname':str},
              values={'nickname' : '^[\w]{1,100}$'
              }
             )
    def check_nickname(self,nickname):
        pass
            #print 'adding user "%s:%s" with email: %s:%s password: %s:%s' % (name, type(name),email, type(email),password, type(password))
    @validate(requires=['password'],
              types={'password':str},
              values={ 'password' : '(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[?!_#%^&@-]).{9,}'
              }
             )
    def check_password(self,password):
        pass
            #print 'adding user "%s:%s" with email: %s:%s password: %s:%s' % (name, type(name),email, type(email),password, type(password))
    @cherrypy.tools.json_in()
    @cherrypy.tools.json_out()
    def GET(self,id):
        
        response = {"user" : {},
                    "errors" : []}
        
        if (('user' not in cherrypy.session) or (cherrypy.session['user']!=int(id))):
            queryGetUser = ("""
                    select id,name,nickname from USERS where id = %(id)s
            """)
        else:
            queryGetUser = ("""
                    select id,name,nickname,email from USERS where id = %(id)s
            """)
        inputDict={
            "id": id
        }
        
        try:
            cnx = mysql.connector.connect(user = DATABASE_USER,host=DATABASE_HOST,database=DATABASE_NAME)
            cursor = cnx.cursor(dictionary = True, buffered= True)
            cursor.execute(queryGetUser,inputDict)
            rows=cursor.rowcount
            if rows>1:
                response['errors'].append(utils.errorObject(000001,': Database Currupted: two users with same id'));
            else:       
                response['user']=cursor.fetchone()
                
        except mysql.connector.errors.IntegrityError:
            response['errors'].append(utils.errorObject(600000));
        except mysql.connector.errors.Error as ex:
            response['errors'].append(utils.errorObject(000001));
            
        return response
    @cherrypy.tools.json_in()
    @cherrypy.tools.json_out()
    def PUT(self,id):
        print "Entered USERSIDPUT"
        #name email password nick passwordConfirm
        data = cherrypy.request.json
       
            
        response = {"user" : {},
                    "errors" : []}
        #( 'user' not in cherrypy.session) or 
        if (( 'user' not in cherrypy.session) or (cherrypy.session['user']!=int(id))):
            response['errors'].append(utils.errorObject(800000));
            return response
        inputDict={"id":id}
        
        
        queryUpdateUser = ("""
                    update USERS set 
            """)
        aux=""
        userM={}
        userM['id']=id
        #checking passwords
        if (("password" in data) and ("passwordConfirm" in data) and (data["password"]) and (data['passwordConfirm'])):
            if data['password']!= data['passwordConfirm']:
                response['errors'].append(utils.errorObject(600001));
                return response
            try:
                self.check_password(password=data['password'])
            except ValidationException as ex:
                print ex
                response['errors'].append(utils.errorObject(600002,": password"));
                return response
            hash = pwd_context.encrypt(data['password'])
            inputDict['password'] = hash
            aux=aux+"password=%(password)s"
            userM['password']=""
        #checking nickname
        if (("nickname" in data) and (data["nickname"])):
            try:
                self.check_nickname(nickname=data['nickname'])
            except ValidationException as ex:
                print ex
                response['errors'].append(utils.errorObject(600002,": nickname"));
                return response
            inputDict['nickname'] = data['nickname']
            if aux:
                aux=aux+","
            aux=aux+"nickname=%(nickname)s"
            userM['nickname']=data['nickname']
        #checking email
        if (("email" in data) and (data["email"])):
            try:
                self.check_email(email=data['email'])
            except ValidationException as ex:
                print ex
                response['errors'].append(utils.errorObject(600002,": email"));
                return response
            inputDict['email'] = data['email']
            if aux:
                aux=aux+","
            aux=aux+"email=%(email)s"
            userM['email']=data['email']
        #checking name
        if (("name" in data) and (data["name"])):
            inputDict['name'] = data['name']
            if aux:
                aux=aux+","
            aux=aux+"name=%(name)s"
            userM['name']=data['name']
        if aux:
            queryUpdateUser = (queryUpdateUser+ " "+aux + " where id=%(id)s")
        else:
            response['errors'].append(utils.errorObject(600002));
            return response
        
        
        try:
            cnx = mysql.connector.connect(user = DATABASE_USER,host=DATABASE_HOST,database=DATABASE_NAME)
            cursor = cnx.cursor(dictionary = True, buffered= True)
            cursor.execute(queryUpdateUser,inputDict)
            rows=cursor.rowcount
            if rows!=1:
                response['errors'].append(utils.errorObject(00001,': User not found'));
            else:
                response['user']=userM
                cnx.commit()
                cnx.close()
                return response 
        except mysql.connector.errors.IntegrityError:
            response['errors'].append(utils.errorObject(600000,"Duplicate email or nickname"));
        except mysql.connector.errors.Error as ex:
            response['errors'].append(utils.errorObject(000001,": really Unknown"));
            
        
    
        return response
