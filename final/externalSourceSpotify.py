import cherrypy
import sys
import os
import jinja2
import spotipy
from externalSource import ExternalSource
trackKeysToKeep = ['duration_ms','href','id','name','popularity']
albumKeysToKeep = ['name','href','id']
artistsKeysToKeep = ['name','href','id']
metaDataKeysToKeep = ['next','href','limit','offset','total','previous']
class ExternalSourceSpotify(ExternalSource):
    def __init__(self):
       self.name="spotify"
    def search(self,query,params=None):
        spotify = spotipy.Spotify()
        processedQuery=""
        if params:
            for parameter in params:
                if parameter == "artist":
                    if processedQuery:
                        processedQuery = processedQuery + " OR "
                    processedQuery=processedQuery+'artist:"'+query+'"'
                elif parameter == "track":
                    if processedQuery:
                        processedQuery = processedQuery + " OR "
                    processedQuery=processedQuery+'track:"'+query+'"'
                elif parameter == "album":
                    if processedQuery:
                        processedQuery = processedQuery + " OR "
                    processedQuery=processedQuery+'album:"'+query+'"'
        else:
            processedQuery=query
        print "Processed Query: "+processedQuery
        results = spotify.search(q=processedQuery, type='track',limit=20)
        #selecting only useful information
        resultsReturn=[]
        for  track in results['tracks']['items']:
            resultsReturn.append({})
            resultsReturn[-1]={key :track[key] for key in trackKeysToKeep }
            if(('external_urls' in track)  and ('spotify' in track['external_urls'])):
                resultsReturn[-1]['href']=track['external_urls']['spotify']
            resultsReturn[-1]['album']={}
            resultsReturn[-1]['artists']=[]
            resultsReturn[-1]['album']={key : track['album'][key] for key in albumKeysToKeep }
            for artist in track['artists']:
                resultsReturn[-1]['artists'].append({})
                resultsReturn[-1]['artists'][-1]={ key : artist[key] for key in artistsKeysToKeep }
        resultsReturnReal={}
        resultsReturnReal['tracks']={}
        resultsReturnReal['tracks']['items']=resultsReturn
        for key in metaDataKeysToKeep:
            resultsReturnReal['tracks'][key]=results['tracks'][key]
        return resultsReturnReal
    def next(self,previousRes,params=None):
        spotify = spotipy.Spotify()
        results=spotify.next(previousRes['tracks'])
        #selecting only useful information
        #selecting only useful information
        resultsReturn=[]
        for  track in results['tracks']['items']:
            resultsReturn.append({})
            resultsReturn[-1]={key :track[key] for key in trackKeysToKeep }
            if(('external_urls' in track)  and ('spotify' in track['external_urls'])):
                resultsReturn[-1]['href']=track['external_urls']['spotify']
            resultsReturn[-1]['album']={}
            resultsReturn[-1]['artists']=[]
            resultsReturn[-1]['album']={key : track['album'][key] for key in albumKeysToKeep }
            for artist in track['artists']:
                resultsReturn[-1]['artists'].append({})
                resultsReturn[-1]['artists'][-1]={ key : artist[key] for key in artistsKeysToKeep }
        resultsReturnReal={}
        resultsReturnReal['tracks']={}
        resultsReturnReal['tracks']['items']=resultsReturn
        for key in metaDataKeysToKeep:
            resultsReturnReal['tracks'][key]=results['tracks'][key]
        return resultsReturnReal