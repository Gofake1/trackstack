import cherrypy
import sys
import os
import jinja2
import mysql.connector
import mysql.connector.errors
import json
from decimal import *
import random
from passlib.apps import custom_app_context as pwd_context
#Define database variables
DATABASE_USER = 'root'
DATABASE_HOST = '127.0.0.1'
DATABASE_NAME = 'TrackStackDB'
from pyvalidate import validate,ValidationException
import utils
sys.stdout = sys.stderr # Turn off console output; it will get logged by Apache

from usersid import UsersID
from usersLogin import UsersLogin
from usersLogout import UsersLogout
class Users(object):
    def __init__(self):
        self.usersid= UsersID()
        self.usersLogin = UsersLogin()
        self.usersLogout= UsersLogout()
        pass
    exposed = True
    def _cp_dispatch(self,vpath):       
        if len(vpath)>1:
            if vpath[0] == 'static':
                return vpath
        if len(vpath) == 0: #/
            print "Entered 0"
            return self
        elif len(vpath)==1: #/login or /logout or /id
            print "entered 1"
            arg1=vpath.pop()
            if arg1=="login":
                return self.usersLogin
            elif arg1=="logout":
                return self.usersLogout
            else:
                try:
                    arg1Int = int(arg1)
                    cherrypy.request.params['id']=arg1
                    return self.usersid
                except:
                    return vpath
            return vpath
        elif len(vpath)==2: # /id/playlists
            print "entered 2"
            resource2=vpath.pop()
            userID=vpath.pop()
            print userID
            print resource2
            try:
                userIDInt = int(userID)
            except:
                return vpath
            cherrypy.request.params['userID']=userID
            if resource2=="playlists":
                return self.usersid.playlists
            else:
                return vpath
        else:
            print "Entered neither"
        return vpath
    @validate(requires=['email', 'password','nickname'],
              types={'email':str, 'password':str,'nickname':str},
              values={'email':'^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$',
                       'password' : '(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[?!_#%^&@-]).{9,}',
                       'nickname' : '^[\w]{1,100}$'
              }
             )
    def check_params(self, email, password,nickname):
        pass
            #print 'adding user "%s:%s" with email: %s:%s password: %s:%s' % (name, type(name),email, type(email),password, type(password))
    
    def GET(self):
        output_format = cherrypy.lib.cptools.accept(['text/html'])
        if output_format == 'text/html':
            return "Put Registration Form here"
        else:
            return "it should never have returned this"
    @cherrypy.tools.json_in()
    @cherrypy.tools.json_out()
    def POST(self):
        print "Entered USERSPUT"
        #name email password nick passwordConfirm
        data = cherrypy.request.json
        response = {"user" : {},
                    "errors" : []}
        if  ((not "password" in data) or (not "email" in data) or (not "nickname" in data) or (not "passwordConfirm" in data) 
                or (not data["password"])or (not data["email"])or (not data["nickname"])or (not data["passwordConfirm"])):
           response['errors'].append(utils.errorObject(200001));
        else:
            #checking passwords
            if data['password']!= data['passwordConfirm']:
                response['errors'].append(utils.errorObject(600001));
            else:
                try:
                    self.check_params(email=data['email'],password=data['password'],nickname=data['nickname'])
                except ValidationException as ex:
                    print ex
                    response['errors'].append(utils.errorObject(600002));
                    return response
                hash = pwd_context.encrypt(data['password'])
                if(not "name" in data):
                    data["name"]=""
                inputDict={
                    'name' : data['name'],
                    'email': data['email'],
                    'password' :hash,
                    'nickname': data['nickname']
                }
                queryInsertUser = ("""
                        insert into USERS(name,email,password,nickname) values(%(name)s,%(email)s,%(password)s,%(nickname)s)

                """)
                try:
                    cnx = mysql.connector.connect(user = DATABASE_USER,host=DATABASE_HOST,database=DATABASE_NAME)
                    cursor = cnx.cursor(dictionary = True, buffered= True)
                    cursor.execute(queryInsertUser,inputDict)
                    rows=cursor.rowcount
                    if rows!=1:
                        response['errors'].append(utils.errorObject(00001,': Error Inserting'));
                    else:       
                        userM={}
                        userM['id']=cursor.lastrowid;
                        userM['name']=data['name']
                        userM['nickname']=data['nickname']
                        userM['email']=data['email']
                        userM['password']=""
                        response['user']=userM
                        cnx.commit()
                        cnx.close()
                        return response 
                except mysql.connector.errors.IntegrityError:
                    response['errors'].append(utils.errorObject(600000));
                except mysql.connector.errors.Error as ex:
                    response['errors'].append(utils.errorObject(000001));
                    
                return response
        
        return response
application = cherrypy.Application(Users(),None) 
conf={'/':{'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
            "tools.sessions.on" : True,
            "tools.sessions.storage_type" : "file",
            "tools.sessions.storage_path" : "/var/www/final/sessions",
            "tools.sessions.timeout" : 15}
}
application.merge(conf)